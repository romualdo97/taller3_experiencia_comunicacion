// metodos del minijuego
function pttrnCheckpoint(obj_data)
{
	this.isChecked = false;
	this.isIterated = false; 

	this.isCheckable = obj_data.isCheckable == undefined ? true : obj_data.isCheckable;
	console.log(this.isCheckable);

	// identificacion
	this.key = obj_data.key;

	// abstract info
	this.value = obj_data.value;

	// transformacion
	this.x = obj_data.x;
	this.y = obj_data.y;  
	this.radius = obj_data.radius;
	
	// visual
	this.hex_color = obj_data.hex_color;
	this.hex_colorSelected = obj_data.hex_colorSelected;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
	this.sprite = new Image();
	if (obj_data.sprite_src)
	{
		this.sprite.src = obj_data.sprite_src;		
	}
}


pttrnCheckpoint.prototype.drawBoundingCircle = function(ctx, hex_color, hex_colorSelected) 
{
	this.hex_color = hex_color;
	this.hex_colorSelected = hex_colorSelected;

	ctx.beginPath();
	ctx.fillStyle = hex_color ? hex_color : this.hex_colorSelected;
	if (this.isChecked) ctx.fillStyle = hex_colorSelected ? hex_colorSelected : this.hex_colorSelected;	
	ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2);
	ctx.fill();
	ctx.closePath();
};

pttrnCheckpoint.prototype.drawBoundingCircle_stroke = function(ctx, hex_color, hex_colorSelected) 
{
	this.hex_color = hex_color;
	this.hex_colorSelected = hex_colorSelected;
	ctx.beginPath();
	ctx.lineWidth = 5;
	context.strokeStyle = hex_color ? hex_color : this.hex_colorSelected;
	if (this.isChecked) ctx.strokeStyle = hex_colorSelected ? hex_colorSelected : this.hex_colorSelected;	
	ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2);
	ctx.stroke();
	ctx.closePath();
};

pttrnCheckpoint.prototype.drawSprite = function(ctx, image) 
{
	// anchor point: botton middle
	if (this.isChecked) ctx.drawImage(this.sprite, this.x - this.sprite.width/2, this.y - this.sprite.height/2);
	// no tener las siguientes lineas afecta el minijuego patrones
		//context.fillStyle = "#fff";
		//context.fillText(this.key, this.x, this.y + 60);
		//context.font = "25pt ritaglio";
		//ctx.fillRect(this.x - this.sprite.width/2, this.y - this.sprite.height/2, this.sprite.width, this.sprite.height);
};

pttrnCheckpoint.prototype.drawValueText = function(ctx, _x, _y, color, font_family) {
	context.fillStyle = color;
	context.font = "25pt " + font_family;
	context.fillText(this.value, this.x + _x, this.y + _y);
};

pttrnCheckpoint.prototype.fillBoundingBox = function(ctx, color) 
{
	context.fillStyle = color;
	context. beginPath()
	context.fillRect(this.x - ((this.radius*2)/2), this.y - ((this.radius*2)/2), this.radius*2, this.radius*2);
	context.closePath();	
};

pttrnCheckpoint.prototype.update = function() 
{
	if (this.isMouseOver() && isMouseDown && this.isCheckable)
	{
		this.isChecked = true;
		// console.log(this.isChecked);
	}


	if (!isMouseDown) 
	{
		this.isChecked = false;
		this.isIterated = false;
	}
};


pttrnCheckpoint.prototype.update2 = function() 
{
	if (this.isMouseOver() && isMouseDown && this.isCheckable)
	{
		this.isChecked = true;
		// console.log(this.isChecked);
	}

	if (!isMouseDown) 
	{
		this.isChecked = false;
		// this.isIterated = false;
	}
};

pttrnCheckpoint.prototype.isMouseOver = function() 
{
	return this.radius > Math.sqrt(Math.pow(this.x - mouseX, 2) + Math.pow(this.y - mouseY, 2));					
};

function Luna(obj_data)
{
	this.x = obj_data.x;
	this.y = obj_data.y;
	this.hex_color = obj_data.hex_color;
}

Luna.prototype.draw = function(context, arc, clockwise) 
{
	context.beginPath();
	context.fillStyle = this.hex_color;
	context.arc(this.x, this.y, 30, 0, arc, clockwise);	
	context.fill();
	context.closePath();	
};