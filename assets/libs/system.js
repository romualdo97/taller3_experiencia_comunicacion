var canvas, context;
var isMouseDown = false;
var mouseX = 0, mouseY = 0;

function enableInputs()
{

	window.addEventListener("keypress", function(e) {
		if ((currentScene == 2 || currentScene == 3) && e.keyCode == 32) 
	    	window.location.assign("movie.html");
	    if (currentScene == 0 && e.keyCode == 32) 
	    	currentScene = 1;
	}, false);
	window.addEventListener("mousemove", function(e) {
		mouseX = e.clientX - canvas.offsetLeft;
		mouseY = e.clientY - canvas.offsetTop;
	}, false);
	window.addEventListener("mousedown", function(e){
		isMouseDown = true;
	}, false);
	window.addEventListener("mouseup", function(e){
		isMouseDown = false;
	}, false);	

	// Mobile input support
	window.addEventListener("touchstart", function(e){
		console.log(e.touches[0]);
		var t = e.touches[0];
		mouseX = t.pageX - canvas.offsetLeft;
		mouseY = t.pageY - canvas.offsetTop;
		isMouseDown = true;

		if (currentScene == 2 || currentScene == 3)
			window.location.assign("movie.html");
	}, false);
	window.addEventListener("touchmove", function(e){
		var t = e.touches[0];
		mouseX = t.pageX - canvas.offsetLeft;
		mouseY = t.pageY - canvas.offsetTop;
	}, false);
	window.addEventListener("touchend", function(e){
		isMouseDown = false;
	}, false);
}