var sprites_folder = "assets/minijuegos/arte/sprites/";

// images
var background = new Image();
background.src = sprites_folder + "bg.png";
var sprite_pincel_amarillo = new Image();
sprite_pincel_amarillo.src = sprites_folder + "pincel_amarillo.png";
var sprite_pincel_azul = new Image();
sprite_pincel_azul.src = sprites_folder + "pincel_azul.png";
var sprite_pincel_rojo = new Image();
sprite_pincel_rojo.src = sprites_folder + "pincel_rojo.png";
var sprite_pincel_neutro = new Image();
sprite_pincel_neutro.src = sprites_folder + "pincel_neutral.png";


// =======================================================================

function MainScene()
{
	this.chkPoints = []; // todos los colores que se puedn combinar
	this.combinationBoard = []; // almacena el color que hay que producir
	this.selectedChkPoints = [];
	this.selectedCombinationBoard = [];

	this.selectedColors4CombinationBoard0 = [];
	this.selectedColors4CombinationBoard1 = [];
	this.selectedColors4CombinationBoard2 = [];

	this.isCorrectPassword = [];
	
	this.password = [3, 5, 1, 4];
	this.canCheckPass = false;

	this.combinacionesCorrectas = [false, false, false]

	this.chkPoints_colors = [
		"#ff2", "#11f", "#f11"
	]
	this.chkPoints_colorsSelected = [
		"#dd2", "#11d", "#d11"
	]

	this.combinationBoard_colors = [
		"#fa0", "#3f0", "#f0c"
	]
	this.combinationBoard_colorsSelected = [
		"#e80", "#1e0", "#e0a"
	]

	// generacion de checkpoints
	for (var i = 0; i < 3; i++)
	{	
		this.chkPoints.push(new pttrnCheckpoint({
			x: i + 260, y: i * 120 + 270, 
			radius: 35, key: i
		}));

		this.combinationBoard.push(new pttrnCheckpoint({
			x: (i + 400) + 260, y: i * 120 + 270,
			radius: 35, key: i
		}));
	}

	// ajustar posicion de algunos colores primarios
	this.chkPoints[0].x += 110;
	this.chkPoints[2].x += 110;

	// ajustar posicion de algunos colores secundarios
	this.combinationBoard[0].x += -40;
	this.combinationBoard[1].x += -110;
	this.combinationBoard[2].x += -80;


}

MainScene.prototype.draw = function(context)
{

	// background for debug purposes
	//context.fillStyle = "#3498db";
	context.fillStyle = "#494949";
	//context.fillRect(0, 0, canvas.width, canvas.height);
	context.drawImage(background, 0, 0);


	for (var i = 0; i < this.chkPoints.length; i++)
	{
		this.chkPoints[i].drawBoundingCircle(context, this.chkPoints_colors[i], this.chkPoints_colorsSelected[i]);		
		this.combinationBoard[i].drawBoundingCircle_stroke(context, this.combinationBoard_colors[i], this.combinationBoard_colorsSelected[i]);
	}
	
	for (var i = 0; i < this.selectedColors4CombinationBoard0.length; i++)
	{
		this.selectedColors4CombinationBoard0[i].draw(context, Math.PI);
		if (i > 0)
			this.selectedColors4CombinationBoard0[i - 1].draw(context, Math.PI, true);
	}
	
	for (var i = 0; i < this.selectedColors4CombinationBoard1.length; i++)
	{
		this.selectedColors4CombinationBoard1[i].draw(context, Math.PI);
		if (i > 0)
			this.selectedColors4CombinationBoard1[i - 1].draw(context, Math.PI, true);
	}

	for (var i = 0; i < this.selectedColors4CombinationBoard2.length; i++)
	{
		this.selectedColors4CombinationBoard2[i].draw(context, Math.PI);
		if (i > 0)
			this.selectedColors4CombinationBoard2[i - 1].draw(context, Math.PI, true);
	}

	dibujarPincel(context, sprite_pincel_neutro);

	if (this.selectedChkPoints.length > 0)
	{
		var lastSelectedChkp = this.selectedChkPoints[0]
		context.beginPath();
		context.strokeStyle = lastSelectedChkp.hex_colorSelected;
		context.lineWidth = 5;
		context.moveTo(lastSelectedChkp.x, lastSelectedChkp.y);
		context.lineTo(mouseX, mouseY);
		context.stroke();
		context.closePath();

		context.beginPath();
		context.fillStyle = lastSelectedChkp.hex_colorSelected;

		context.arc(mouseX, mouseY, 20, 0, Math.PI*2, false);

		context.fill();
		context.closePath();

		if (lastSelectedChkp.hex_color == "#ff2")
			dibujarPincel(context, sprite_pincel_amarillo);
		if (lastSelectedChkp.hex_color == "#11f")
			dibujarPincel(context, sprite_pincel_azul);
		if (lastSelectedChkp.hex_color == "#f11")
			dibujarPincel(context, sprite_pincel_rojo);						
	}	

}

MainScene.prototype.update = function() 
{
	
	for (var i = 0; i < this.chkPoints.length; i++)
	{
		this.chkPoints[i].update();
		if (this.chkPoints[i].isChecked && !this.chkPoints[i].isIterated) 
		{
			this.chkPoints[i].isIterated = true;
			this.selectedChkPoints.push(this.chkPoints[i]);
			// mantener en el array de elementos seleccionados
			// solo el ultimo  elemento seleccionado
			if (this. selectedChkPoints.length > 1) 
			{
			 	// temp for Selected CheckPoints
				var tmp_scp = this.selectedChkPoints;
				for (var i = 0; i < this.selectedChkPoints.length; i++)
				{
					this.selectedChkPoints[i].isChecked = false;
					this.selectedChkPoints[i].isIterated = false;
				}
				this.selectedChkPoints = [];
				this.selectedChkPoints.push(tmp_scp[tmp_scp.length - 1]);
				this.selectedChkPoints[0].isChecked = true;
				this.selectedChkPoints[0].isIterated =  true;
			}
		}

		this.combinationBoard[i].update();
		if (this.combinationBoard[i].isChecked && !this.combinationBoard[i].isIterated) 
		{
			this.combinationBoard[i].isIterated = true;
			this.selectedCombinationBoard.push(this.combinationBoard[i]);
			

			// mantener en el array de elementos seleccionados
			// solo el ultimo  elemento seleccionado
			if (this.selectedCombinationBoard.length > 1) 
			{
			 	// temp for Selected CheckPoints
				var tmp_scp = this.selectedCombinationBoard;
				console.log(this.selectedCombinationBoard);
				for (var i = 0; i < this.selectedCombinationBoard.length; i++)
				{
					this.selectedCombinationBoard[i].isChecked = false;
					this.selectedCombinationBoard[i].isIterated = false;
				}

				this.selectedCombinationBoard = [];
				this.selectedCombinationBoard.push(tmp_scp[tmp_scp.length - 1]);
				this.selectedCombinationBoard[0].isChecked = true;
				this.selectedCombinationBoard[0].isIterated =  true;
				tmp_scp = [];
			}

			if (this.selectedChkPoints.length >= 1)
			{				
				if (this.selectedCombinationBoard[0].key == 0)
				{
					this.selectedColors4CombinationBoard0.push(new Luna({
						x: this.selectedCombinationBoard[0].x,
						y: this.selectedCombinationBoard[0].y,
						hex_color: this.selectedChkPoints[0].hex_color
					}));				
				}

				if (this.selectedCombinationBoard[0].key == 1)
				{
					this.selectedColors4CombinationBoard1.push(new Luna({
						x: this.selectedCombinationBoard[0].x,
						y: this.selectedCombinationBoard[0].y,
						hex_color: this.selectedChkPoints[0].hex_color
					}));				
				}

				if (this.selectedCombinationBoard[0].key == 2)
				{
					this.selectedColors4CombinationBoard2.push(new Luna({
						x: this.selectedCombinationBoard[0].x,
						y: this.selectedCombinationBoard[0].y,
						hex_color: this.selectedChkPoints[0].hex_color
					}));				
				}
			}

			
//			console.log(this.selectedCombinationBoard);
		}
	}
	if (!isMouseDown) 
	{
		if (this.selectedColors4CombinationBoard0.length > 1)
		{
			var colors4cb0 = this.selectedColors4CombinationBoard0;
			if ((colors4cb0[colors4cb0.length - 1].hex_color == "#ff2" && 
				colors4cb0[colors4cb0.length - 2].hex_color == "#f11") ||
				(colors4cb0[colors4cb0.length - 1].hex_color == "#f11" && 
				colors4cb0[colors4cb0.length - 2].hex_color == "#ff2"))
				{	
					this.combinacionesCorrectas[0] = true;
					console.log("color naranja es correcto");
				}
		}

		if (this.selectedColors4CombinationBoard1.length > 1)
		{
			var colors4cb1 = this.selectedColors4CombinationBoard1;
			if ((colors4cb1[colors4cb1.length - 1].hex_color == "#ff2" && 
				colors4cb1[colors4cb1.length - 2].hex_color == "#11f") ||
				(colors4cb1[colors4cb1.length - 1].hex_color == "#11f" && 
				colors4cb1[colors4cb1.length - 2].hex_color == "#ff2"))
				{
					this.combinacionesCorrectas[1] = true;
					console.log("color verde es correcto");					
				}
 	 	}          

 	 	if (this.selectedColors4CombinationBoard2.length > 1)
		{
			var colors4cb2 = this.selectedColors4CombinationBoard2;
			if ((colors4cb2[colors4cb2.length - 1].hex_color == "#11f" && 
				colors4cb2[colors4cb2.length - 2].hex_color == "#f11") ||
				(colors4cb2[colors4cb2.length - 1].hex_color == "#f11" && 
				colors4cb2[colors4cb2.length - 2].hex_color == "#11f"))
				{	
					this.combinacionesCorrectas[2] = true;
					console.log("color purpura es correcto");
				}
 	 	}

		this.selectedChkPoints = [];
		this.selectedCombinationBoard = [];
	}

	for (var i = 0; i < 3; i++)
	{
		if (!this.combinacionesCorrectas[i]) return;
	}

	currentScene = GAME_WIN;

}

function dibujarPincel(context, sprite_pincel)
{
	context.fillStyle = "#000";
	var x = mouseX - 80;
	var y = (mouseY - sprite_pincel.height) + 50;
	//context.fillRect(x, y, sprite_pincel_amarillo.width, sprite_pincel_amarillo.height);
	context.drawImage(sprite_pincel, x, y);

}

function VictoryScene()
{}

VictoryScene.prototype.draw = function(context) 
{
	context.fillStyle = "#2ecc71";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "40pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Ve por la puerta verde!!!", 210, 370);
};

VictoryScene.prototype.update = function()
{}

function DefeatScene()
{}

DefeatScene.prototype.draw = function(context) 
{
	context.fillStyle = "#e74c3c";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "25pt ritaglio";
	context.fillStyle = "#000";
	context.fillText("Sigue ejercitando tu memoria!!!", 230, 177);
	context.fillStyle = "#fff";
	context.fillText("Sigue ejercitando tu memoria!!!", 230, 175);

	context.font = "18pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("(Presiona space para volver a empezar)", 245, 300);
};

DefeatScene.prototype.update = function()
{}


function InstructionsScene()
{}

InstructionsScene.prototype.draw = function(context) 
{
	context.fillStyle = "#e74c3c";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "45pt ritaglio";
	context.fillStyle = "#000";
	context.fillText("Instrucciones", 230, 177);
	context.fillStyle = "#fff";
	context.fillText("Instrucciones", 230, 175);

	context.font = "18pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Dejar presionado el mouse y seleccionar las personas", 245, 200);
	context.fillText("en el orden que memorizaste los circilos verdes", 245, 225);
	//las personas en el orden que memorizaste los circilos verdes

	context.font = "14pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("(Presiona space para empezar)", 245, 300);
};

InstructionsScene.prototype.update = function()
{}