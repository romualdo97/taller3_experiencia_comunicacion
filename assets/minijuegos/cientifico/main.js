/*var canvas, context;
var isMouseDown = false;
var mouseX = 0, mouseY = 0;*/

// scenes
var scenes = [], currentScene = 1; // 0 == MainScene
var gameStatus = 0;
var GAME_RUNNING = 1, GAME_WIN = 2, GAME_LOSS = 3;

// metodos del sistema
function instantiateWorldClasses()
{
	canvas = document.getElementById("minijuego_patrones");
	context = canvas.getContext("2d");
	scenes.push(new InstructionsScene());
	scenes.push(new MainScene());
	scenes.push(new VictoryScene());
	scenes.push(new DefeatScene());
	// scenes[i],   i == 0 : instructions scene
	// 				i == 1 : gameplay scene
	//				i == 2 : win scene
	//				i == 3 : lose scene
}

function init()
{
	instantiateWorldClasses();
	enableInputs();
	run();
}

function run()
{
	//currentScene = 2;
	scenes[currentScene].update();
	scenes[currentScene].draw(context)
	requestAnimationFrame(run)
}

window.addEventListener("DOMContentLoaded", init, false);