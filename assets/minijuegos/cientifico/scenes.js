var sprites_folder = "assets/minijuegos/cientifico/sprites/";

// images
var background = new Image();
background.src = sprites_folder +"bg.jpg";

// =======================================================================

function MainScene()
{
	this.chkPoints = [];
	this.selectedChkPoints = [];
	this.isCorrectPassword = [];
	this.password = [0, 6, 12, 18, 24];
	this.canCheckPass = false;

	this.sum4Victory = 7;

	// para mantener dibujadas las lineas
	this.backCanvas = document.createElement("canvas");
	this.backCanvas.width = canvas.width;
	this.backCanvas.height = canvas.height;
	this.backContext = this.backCanvas.getContext("2d"); // context canvas4lines
	this.linesHistorial = [];

	// TIMERS
	// timer para animar el alpha del boton inicial
	this.timer = 1;

	// los valores q tendran los chkpnts
	this.values4chkpoints = [0, -1, 2, 1, -3, 
							3, -3, -1, 2, -6,
							-2, 1, -1, -1, -2,
							6, -3, -3, 2, -2,
							-1, -3, 2, -2, 0];

	// crear chckpoints
	this.LEFT_MARGIN = 249;
	this.TOP_MARGIN = 165;
	for (var i = 0; i < 25; i++)
	{
		this.chkPoints.push(new pttrnCheckpoint({// 35
			x: this.LEFT_MARGIN + (Math.floor(i/5) * (120)),
			y: this.TOP_MARGIN + ((i%5) * (96)),
			radius: 35, sprite_src: sprites_folder + "chkp0.png", key: i,
			value: this.values4chkpoints[i], 
			isCheckable: false
		}));
	}

	// configurando el chckpoint inicial
	this.chkPoints[0].value = "Inicio";
	this.chkPoints[0].isCheckable = true;

	// configurando el chckpoint final
	this.chkPoints[this.chkPoints.length - 1].value = "Salida";
}

MainScene.prototype.draw = function(context)
{
	// dibujar background
	context.fillStyle = "#494949";
	context.fillRect(0, 0, canvas.width, canvas.height);
	context.drawImage(background, 0, -2);

	// dibujar sprites seleccionados
	for (var i = 0; i < this.selectedChkPoints.length; i++)
	{
		this.selectedChkPoints[i].fillBoundingBox(context, "rgba(138, 85, 33, 0.8)");
	}

	// si ya se selecciono el chkpnt inicio
	if (this.selectedChkPoints.length > 0)
	{
		// resaltar los otros chkpnt
		for (var i = 0; i < this.chkPoints.length; i++)
		{
			this.chkPoints[i].fillBoundingBox(context, "rgba(138, 85, 33, 0.3)");
		}
	} 
	else
	{
		// hacer parpadear chkpoint "inicio"
		var alpha = .8 - Math.abs(.5 - this.timer);
		this.chkPoints[0].fillBoundingBox(context, "rgba(138, 85, 33, " + alpha + ")");
		if (this.chkPoints[0].isChecked)
		{
			this.chkPoints[0].fillBoundingBox(context, "rgba(0, 0, 0, 0.5)");
		}		
	}


	// dibujar checkpoint "salida"
	
	/*this.chkPoints[this.chkPoints.length - 1].fillBoundingBox(context, "#f00");
	this.chkPoints[this.chkPoints.length - 1].drawValueText(context, - 43, 10, "#fff", "ritaglio");
	if (this.chkPoints[this.chkPoints.length - 1].isChecked)
	{
		this.chkPoints[this.chkPoints.length - 1].fillBoundingBox(context, "rgba(0, 0, 0, 0.4)");
	}*/

	// si hay mas de un checkpoint seleccionado...
	if (this.selectedChkPoints.length > 0)
	{
		var lastSelectedChkp = this.selectedChkPoints[this.selectedChkPoints.length - 1]

		// configurar contexto
		context.strokeStyle = "rgba(200, 146, 84, 1)";
		context.lineWidth = 5;

		// dibujar linea entre lastSelectedChkp y mouse
		context.beginPath();
		context.moveTo(lastSelectedChkp.x, lastSelectedChkp.y);
		context.lineTo(mouseX, mouseY);
		context.stroke();
		context.closePath();

		// dibujar circulo alrededor del mouse
		context.beginPath();
		context.fillStyle = "rgba(200, 146, 84, 1)";
		context.arc(mouseX, mouseY, 20, 0, Math.PI*2, false);
		context.fill();
		context.closePath();
	}
	
	// si se han seleccionado dos checkpoints...
	if (this.linesHistorial.length == 2)
	{
		// configurar contexto
		this.backContext.strokeStyle = "#000";
		this.backContext.strokeStyle = "rgba(200, 146, 84, 1)";
		this.backContext.lineWidth = 5;
		
		// dibujar lineas entre checkpoints
		this.backContext.moveTo(this.linesHistorial[this.linesHistorial.length - 2].x, 
						this.linesHistorial[this.linesHistorial.length - 2].y);
		this.backContext.lineTo(this.linesHistorial[this.linesHistorial.length - 1].x, 
						this.linesHistorial[this.linesHistorial.length - 1].y);

		this.backContext.stroke();
	}
	// dibujar imagen q mantiene el registro de las lineas seleccionadas
	context.drawImage(this.backCanvas, 0, 0);
}

MainScene.prototype.update = function() 
{
	// timer para animar el alpha del boton inicial
	this.timer += .02;
	if (this.timer >= .8)
	{
		this.timer = 0;
	}

	// si ya se selecciono el chkpnt inicio
	if (this.selectedChkPoints.length > 0)
	{
		// permitir q los otros chpnt se seleccionen
		for (var i = 0; i < this.chkPoints.length; i++)
		{
			this.chkPoints[i].isCheckable = true;
		}
	}

	for (var i = 0; i < this.chkPoints.length; i++)
	{
		this.chkPoints[i].update2();
		if (this.chkPoints[i].isChecked && !this.chkPoints[i].isIterated) 
		{
			this.chkPoints[i].isIterated = true;
			console.log("checkpoint["+ i +"] = " + this.chkPoints[i].value);
			this.selectedChkPoints.push(this.chkPoints[i]);

			// mantiene registro de los dos ultimos checkpoints seleccionados
			this.linesHistorial.push(this.chkPoints[i]);
		}
	}

	// cuando linesHistorial tiene mas de dos elementos...
	if (this.linesHistorial.length > 2)
	{
		// remover del array el primer chkpoint ingresado e ingresarle
		// el penultimo y el ultimo chkpoint seleccionado
		var lines_tmp = this.linesHistorial;
		this.linesHistorial = [];
		this.linesHistorial.push(lines_tmp[lines_tmp.length - 2]);
		this.linesHistorial.push(lines_tmp[lines_tmp.length - 1]);
	}

	// cuando evaluar la condicion de victoria?
	// 		- cuando se suelta el mouse
	//		 	- primer checkpoint seleccionad o con id == 0
	// 			- en el checkpoint "salida" con id == 24
	// que se evalua en la condicion de victoria?
	// 		- que todos los checkpoint seleccionados sumen en total sum4Vicotory
	
	// si el click izquierto no esta presionado...
	if (!isMouseDown) 
	{
		var seSeleccionoChkpntSalidaDeUltimo = false;
		var seSeleccionoChkpntInicioDePrimero = false;

		// si se ha seleccionado mas de un chkpoint...
		if (this.selectedChkPoints.length > 0)
		{
			seSeleccionoChkpntSalidaDeUltimo = this.selectedChkPoints[this.selectedChkPoints.length - 1].key == 24;
			seSeleccionoChkpntInicioDePrimero = this.selectedChkPoints[0].key == 0;			
		}

		// si el ultimo elemento seleccionado tiene id == 24...
		// si el primer elemento seleccionado tiene id == 0...
		if (seSeleccionoChkpntInicioDePrimero && seSeleccionoChkpntSalidaDeUltimo)
		{
			var _suma = 0;

			// sumar todos los valores de todos los chkpnts seleccionados
			for (var i = 0;  i < this.selectedChkPoints.length; i++)
			{
				// no sume ni el primer ni el ultimo elemento seleccionado
				if (i == 0 || i == this.selectedChkPoints.length - 1) continue;

				_suma += this.selectedChkPoints[i].value;
			}

			console.log("suma total: " + _suma);
			if (_suma == this.sum4Victory) 
			{
				currentScene = GAME_WIN;
			}
			else
			{
				currentScene = GAME_LOSS;			
			}
		}

		// this.selectedChkPoints = [];
	}

}

function VictoryScene()
{}

VictoryScene.prototype.draw = function(context) 
{
		context.fillStyle = "#2ecc71";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "40pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Ve por la puerta verde!!!", 210, 370);
};

VictoryScene.prototype.update = function()
{}

function DefeatScene()
{}

DefeatScene.prototype.draw = function(context) 
{
	context.fillStyle = "#e74c3c";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "40pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Ve por la puerta roja!!!", 210, 370);
};

DefeatScene.prototype.update = function()
{}

function InstructionsScene()
{}

InstructionsScene.prototype.draw = function(context) 
{
	context.fillStyle = "#e74c3c";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "45pt ritaglio";
	context.fillStyle = "#000";
	context.fillText("Instrucciones", 230, 177);
	context.fillStyle = "#fff";
	context.fillText("Instrucciones", 230, 175);

	context.font = "18pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Dejar presionado el mouse y seleccionar las personas", 245, 200);
	context.fillText("en el orden que memorizaste los circilos verdes", 245, 225);
	//las personas en el orden que memorizaste los circilos verdes

	context.font = "14pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("(Presiona space para empezar)", 245, 300);
};

InstructionsScene.prototype.update = function()
{}