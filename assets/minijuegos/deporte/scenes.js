var sprites_folder = "assets/minijuegos/deporte/sprites/";

// images
var background = new Image();
background.src = sprites_folder +"bg.jpg";

// =======================================================================

function MainScene()
{
	this.chkPoints = [];
	this.selectedChkPoints = [];
	this.isCorrectPassword = [];
	this.password = [0, 5, 6, 11, 12, 17, 18, 23, 24, 29];
	this.canCheckPass = false;
	// imagen solucion
	this.imageSolution = new Image();
	this.imageSolution.src = sprites_folder + "solution.png";
	this.alphaDelta = .005;
	this.alphaSolution = 1;

	// para mantener dibujadas las lineas
	this.backCanvas = document.createElement("canvas");
	this.backCanvas.width = canvas.width;
	this.backCanvas.height = canvas.height;
	this.backContext = this.backCanvas.getContext("2d"); // context canvas4lines
	this.linesHistorial = [];

	// crear chckpoints
	this.LEFT_MARGIN = 185;
	this.TOP_MARGIN = 165;
	for (var i = 0; i < 30; i++)
	{
		this.chkPoints.push(new pttrnCheckpoint({// 35
			x: this.LEFT_MARGIN + (Math.floor(i/5) * (120)),
			y: this.TOP_MARGIN + ((i%5) * (96)),
			radius: 35, sprite_src: sprites_folder + "chkp0.png", key: i,
			value: i, 
			//isCheckable: false
		}));
	}
}

MainScene.prototype.draw = function(context)
{
	// dibujar background
	context.fillStyle = "#494949";
	//context.fillRect(0, 0, canvas.width, canvas.height);
	context.drawImage(background, 0, 0);
    
	// dibujar sprites/chkpoints
	for (var i = 0; i < this.chkPoints.length; i++)
	{
       this.chkPoints[i].drawBoundingCircle(context, "rgba(37, 23, 10, 1)", "rgba(0, 0, 0, .8)");
       //this.chkPoints[i].drawValueText(context, 0, 0, "#fff", "ritaglio");
	}

	// si hay mas de un checkpoint seleccionado...
	if (this.selectedChkPoints.length > 0)
	{
		var lastSelectedChkp = this.selectedChkPoints[this.selectedChkPoints.length - 1]
		
		// configurar contexto
		context.strokeStyle = "rgba(181, 131, 55, 1)";
		context.lineWidth = 8;
        context.lineCap = 'round';

        // dibujar linea entre lastSelectedChkp y mouse
		context.beginPath();
		context.moveTo(lastSelectedChkp.x, lastSelectedChkp.y);
		context.lineTo(mouseX, mouseY);
		context.stroke();
		context.closePath();

		// dibujar circulo alrededor del mouse
		context.beginPath();
		context.arc(mouseX, mouseY, 20, 0, Math.PI*2, false);
		context.fill();
		context.closePath();
	}

	// si se han seleccionado dos checkpoints...
	if (this.linesHistorial.length == 2)
	{
		// configurar contexto
		this.backContext.strokeStyle = "rgba(181, 131, 55, 1)";
		this.backContext.lineWidth = 5;
		
		// dibujar lineas entre checkpoints
		this.backContext.moveTo(this.selectedChkPoints[this.selectedChkPoints.length - 2].x, 
						this.selectedChkPoints[this.selectedChkPoints.length - 2].y);
		this.backContext.lineTo(this.selectedChkPoints[this.selectedChkPoints.length - 1].x, 
						this.selectedChkPoints[this.selectedChkPoints.length - 1].y);
		this.backContext.stroke();
	}
	
	// this.drawSolution(context);
	context.globalAlpha = this.alphaSolution;
	context.drawImage(this.imageSolution, 0, 0);
	// restablece el valor a su estado anterior
	context.globalAlpha = 1;

	// dibujar imagen q mantiene el registro de las lineas seleccionadas
	context.drawImage(this.backCanvas, 0, 0);
}

MainScene.prototype.update = function() 
{
	if  (this. alphaSolution > 0)
		this.alphaSolution -= this.alphaDelta;
	else 
		this.alphaSolution = 0; 

	for (var i = 0; i < this.chkPoints.length; i++)
	{
		this.chkPoints[i].update();
		if (this.chkPoints[i].isChecked && !this.chkPoints[i].isIterated) 
		{
			this.chkPoints[i].isIterated = true;
			this.selectedChkPoints.push(this.chkPoints[i]);

			// mantiene registro de los dos ultimos checkpoints seleccionados
			this.linesHistorial.push(this.chkPoints[i]);
		}
	}

	// cuando linesHistorial tiene mas de dos elementos...
	if (this.linesHistorial.length > 2)
	{
		// remover del array el primer chkpoint ingresado e ingresarle
		// el penultimo y el ultimo chkpoint seleccionado
		var lines_tmp = this.linesHistorial;
		this.linesHistorial = [];
		this.linesHistorial.push(lines_tmp[lines_tmp.length - 2]);
		this.linesHistorial.push(lines_tmp[lines_tmp.length - 1]);
	}
	
	if (!isMouseDown) 
	{
		console.log("no mouse down");
		if (this.selectedChkPoints.length != this.password.length && 
			this.selectedChkPoints.length > 0) 
		{

			currentScene = 3;
			return;
			this.canCheckPass = true;
			//alert(123123);
		}

		if (this.selectedChkPoints.length == this.password.length)
		{
			for (var i = 0; i < this.password.length; i++) 
			{
				this.isCorrectPassword.push((this.selectedChkPoints[i].key == this.password[i]) ? true : false);
				console.log(this.isCorrectPassword);
				// this.isCorrectPassword[i] = (this.selectedChkPoints[i].key == this.password[i]) ? true : false;
			}
			// this.canCheckPass = false;
		}

		// que hacer cuando se gana o se pierde
		for (var i = 0; i < this.isCorrectPassword.length; i++)
		{
			if (!this.isCorrectPassword[i]) 
			{
				// perdiste el juego
				currentScene = 3;
				//gameStatus = GAME_LOSS;
				this.selectedChkPoints = [];
				break;
	        } 

        	// ganaste el juego
        	currentScene = 2;
        	//gameStatus = GAME_WIN;
		}

		this.selectedChkPoints = [];
	}

}

MainScene.prototype.drawSolution = function(ctx) 
{
	// configuar contexto
	ctx.strokeStyle = "#fff";
	ctx.lineWidth = 5;
	drawLineBetween(this.chkPoints[0], this.chkPoints[5], ctx);
	drawLineBetween(this.chkPoints[5], this.chkPoints[6], ctx);
	drawLineBetween(this.chkPoints[6], this.chkPoints[11], ctx);
	drawLineBetween(this.chkPoints[11], this.chkPoints[12], ctx);
	drawLineBetween(this.chkPoints[12], this.chkPoints[17], ctx);
	drawLineBetween(this.chkPoints[17], this.chkPoints[18], ctx);
	drawLineBetween(this.chkPoints[18], this.chkPoints[23], ctx);
	drawLineBetween(this.chkPoints[23], this.chkPoints[24], ctx);
	drawLineBetween(this.chkPoints[24], this.chkPoints[29], ctx);
	ctx.stroke();
};


function drawLineBetween(point1, point2, ctx)
{
	ctx.moveTo(point1.x, point1.y);
	ctx.lineTo(point1.x + (point2.x - point1.x),
				point1.y + (point2.y - point1.y));
}


function VictoryScene()
{}

VictoryScene.prototype.draw = function(context) 
{
	context.fillStyle = "#2ecc71";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "40pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Ve por la puerta verde!!!", 210, 370);
	//context.fillStyle = "#fff";
	//context.fillText("Que buen cerebro tienes, lo has conseguido!!!", 150, 175);

	//context.font = "18pt ritaglio";
	//context.fillStyle = "#fff";
	//context.fillText("(Presiona space para volver a empezar)", 245, 300);
};

VictoryScene.prototype.update = function()
{}

function DefeatScene()
{}

DefeatScene.prototype.draw = function(context) 
{
	context.fillStyle = "#e74c3c";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "40pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Ve por la puerta roja!!!", 210, 370);
};

DefeatScene.prototype.update = function()
{}


function InstructionsScene()
{}

InstructionsScene.prototype.draw = function(context) 
{
	context.fillStyle = "#e74c3c";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "45pt ritaglio";
	context.fillStyle = "#000";
	context.fillText("Instrucciones", 230, 177);
	context.fillStyle = "#fff";
	context.fillText("Instrucciones", 230, 175);

	context.font = "18pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Dejar presionado el mouse y seleccionar las personas", 245, 200);
	context.fillText("en el orden que memorizaste los circilos verdes", 245, 225);
	//las personas en el orden que memorizaste los circilos verdes

	context.font = "14pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("(Presiona space para empezar)", 245, 300);
};

InstructionsScene.prototype.update = function()
{}