var sprites_folder = "assets/minijuegos/patrones/sprites/";

// images
var background = new Image();
background.src = sprites_folder +"bg.png";

// =======================================================================

function MainScene()
{
	this.chkPoints = [];
	this.selectedChkPoints = [];
	this.isCorrectPassword = [];
	this.password = [3, 5, 1, 4];
	this.canCheckPass = false;

	// para mantener dibujadas las lineas
	this.backCanvas = document.createElement("canvas");
	this.backCanvas.width = canvas.width;
	this.backCanvas.height = canvas.height;
	this.backContext = this.backCanvas.getContext("2d"); // context canvas4lines
	this.linesHistorial = [];

	this.chkPoints[0] = new pttrnCheckpoint({
		x: 126, y: 197, radius: 80, sprite_src: sprites_folder + "chkp0.png", key: 1
	});
	this.chkPoints[1] = new pttrnCheckpoint({
		x: 377, y: 338, radius: 35, sprite_src: sprites_folder + "chkp1.png", key: 2
	});
	this.chkPoints[2] = new pttrnCheckpoint({
		x: 572.50, y: 268, radius: 35, sprite_src: sprites_folder + "chkp2.png", key: 3
	});
	this.chkPoints[3] = new pttrnCheckpoint({
		x: 583, y: 92, radius: 35, sprite_src: sprites_folder + "chkp3.png", key: 4
	});
	this.chkPoints[4] = new pttrnCheckpoint({
		x: 705.50, y: 62, radius: 35, sprite_src: sprites_folder + "chkp4.png", key: 5
	});
}

MainScene.prototype.draw = function(context)
{
	// dibujar background
	context.fillStyle = "#494949";
	context.fillRect(0, 0, canvas.width, canvas.height);
	context.drawImage(background, 0, -2);  // clear buffer

	// dibujar sprites
	for (var i = 0; i < this.chkPoints.length; i++)
	{
		this.chkPoints[i].drawSprite(context);
		//this.chkPoints[i].drawBoundingCircle(context, "#fff", "#3498db");
	}

	// si hay mas de un checkpoint seleccionado...
	if (this.selectedChkPoints.length > 0)
	{
		var lastSelectedChkp = this.selectedChkPoints[this.selectedChkPoints.length - 1]

		// configurar contexto
		context.strokeStyle = "#f0f0f0";
		context.lineWidth = 5;

		// dibujar linea entre lastSelectedChkp y mouse
		context.beginPath();
		context.moveTo(lastSelectedChkp.x, lastSelectedChkp.y);
		context.lineTo(mouseX, mouseY);
		context.stroke();
		context.closePath();

		// dibujar circulo alrededor del mouse
		context.beginPath();
		context.arc(mouseX, mouseY, 20, 0, Math.PI*2, false);
		context.fill();
		context.closePath();
	}

	// si se han seleccionado dos checkpoints...
	if (this.linesHistorial.length == 2)
	{
		// configurar contexto
		this.backContext.strokeStyle = "#000";
		this.backContext.strokeStyle = "#fff";
		this.backContext.lineWidth = 5;
		// dibujar lineas entre checkpoints
		this.backContext.moveTo(this.selectedChkPoints[this.selectedChkPoints.length - 2].x, 
						this.selectedChkPoints[this.selectedChkPoints.length - 2].y);
		this.backContext.lineTo(this.selectedChkPoints[this.selectedChkPoints.length - 1].x, 
						this.selectedChkPoints[this.selectedChkPoints.length - 1].y);
		this.backContext.stroke();
	}
	// dibujar imagen q mantiene el registro de las lineas seleccionadas
	context.drawImage(this.backCanvas, 0, 0);
} 

MainScene.prototype.update = function() 
{
	for (var i = 0; i < this.chkPoints.length; i++)
	{
		this.chkPoints[i].update();
		if (this.chkPoints[i].isChecked && !this.chkPoints[i].isIterated) 
		{
			this.chkPoints[i].isIterated = true;
			this.selectedChkPoints.push(this.chkPoints[i]);
			
			// mantiene registro de los dos ultimos checkpoints seleccionados
			this.linesHistorial.push(this.chkPoints[i]);
		}
	}

	// cuando linesHistorial tiene mas de dos elementos...
	if (this.linesHistorial.length > 2)
	{
		// remover del array el primer chkpoint ingresado e ingresarle
		// el penultimo y el ultimo chkpoint seleccionado
		var lines_tmp = this.linesHistorial;
		this.linesHistorial = [];
		this.linesHistorial.push(lines_tmp[lines_tmp.length - 2]);
		this.linesHistorial.push(lines_tmp[lines_tmp.length - 1]);
	}

	if (!isMouseDown) 
	{
		if (this.selectedChkPoints.length != this.password.length && 
			this.selectedChkPoints.length > 0) 
		{
			currentScene = 3;
			return;
		}

		if (this.selectedChkPoints.length == this.password.length)
		{
			for (var i = 0; i < this.password.length; i++) 
			{
				this.isCorrectPassword.push((this.selectedChkPoints[i].key == this.password[i]) ? true : false);
			}
		}

		// que hacer cuando se gana o se pierde
		for (var i = 0; i < this.isCorrectPassword.length; i++)
		{
			if (!this.isCorrectPassword[i]) 
			{
				// perdiste el juego
				currentScene = 3;
				this.selectedChkPoints = [];
				break;
	        } 

        	// ganaste el juego
        	currentScene = 2;
		}

		this.selectedChkPoints = [];
	}

	console.log("Lines historial");
	console.log(this.linesHistorial);
}

function VictoryScene()
{}

VictoryScene.prototype.draw = function(context) 
{
	context.fillStyle = "#2ecc71";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "25pt ritaglio";
	context.fillStyle = "#000";
	context.fillText("Que buen cerebro tienes, lo has conseguido!!!", 150, 177);
	context.fillStyle = "#fff";
	context.fillText("Que buen cerebro tienes, lo has conseguido!!!", 150, 175);

	context.font = "18pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("(Presiona space para volver a empezar)", 245, 300);
};

VictoryScene.prototype.update = function()
{}

function DefeatScene()
{}

DefeatScene.prototype.draw = function(context) 
{
	context.fillStyle = "#e74c3c";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "25pt ritaglio";
	context.fillStyle = "#000";
	context.fillText("Sigue ejercitando tu memoria!!!", 230, 177);
	context.fillStyle = "#fff";
	context.fillText("Sigue ejercitando tu memoria!!!", 230, 175);

	context.font = "18pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("(Presiona space para volver a empezar)", 245, 300);
};

DefeatScene.prototype.update = function()
{}


function InstructionsScene()
{}

InstructionsScene.prototype.draw = function(context) 
{
	context.fillStyle = "#e74c3c";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "45pt ritaglio";
	context.fillStyle = "#000";
	context.fillText("Instrucciones", 230, 177);
	context.fillStyle = "#fff";
	context.fillText("Instrucciones", 230, 175);

	context.font = "18pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Dejar presionado el mouse y seleccionar las personas", 245, 200);
	context.fillText("en el orden que memorizaste los circilos verdes", 245, 225);
	//las personas en el orden que memorizaste los circilos verdes

	context.font = "14pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("(Presiona space para empezar)", 245, 300);
};

InstructionsScene.prototype.update = function()
{}