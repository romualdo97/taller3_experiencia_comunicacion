var sprites_folder = "assets/minijuegos/politica/sprites/";

// images
var background = new Image();
background.src = sprites_folder +"bg.jpg";

// =======================================================================

function MainScene()
{
	this.chkPoints = [];
	this.selectedChkPoints = [];
	this.isCorrectPassword = [];
	//this.password = [7,8,9,4,5,12,18,11,10,15,16,17,22,27,26,20,21];
	this.password = [1, 6, 11, 15, 20, 26, 27, 21, 16, 12, 17, 22, 18, 14, 9, 8, 13];
	this.canCheckPass = false;
	// imagen solucion
	this.imageSolution = new Image();
	this.imageSolution.src = sprites_folder + "solution.png";
	this.alphaDelta = .005;
	this.alphaSolution = 1;

	// para mantener dibujadas las lineas
	this.backCanvas = document.createElement("canvas");
	this.backCanvas.width = canvas.width;
	this.backCanvas.height = canvas.height;
	this.backContext = this.backCanvas.getContext("2d"); // context canvas4lines
	this.linesHistorial = [];

	// crear chckpoints
	this.LEFT_MARGIN = 185;
	this.TOP_MARGIN = 165;
	for (var i = 0; i < 30; i++)
	{
		this.chkPoints.push(new pttrnCheckpoint({// 35
			x: this.LEFT_MARGIN + (Math.floor(i/5) * (120)),
			y: this.TOP_MARGIN + ((i%5) * (96)),
			radius: 35, sprite_src: sprites_folder + "chkp0.png", key: i,
			value: i, 
			//isCheckable: false
		}));
	}

}

MainScene.prototype.draw = function(context)
{
	// dibujar background
	context.fillStyle = "#494949";
	//context.fillRect(0, 0, canvas.width, canvas.height);
	context.drawImage(background, 0, -2);

	// dibujar sprites/chkpoints
	//context.strokeStyle = "#fff";
	for (var i = 0; i < this.chkPoints.length; i++)
	{
		this.chkPoints[i].drawBoundingCircle(context, "rgba(92, 67, 29, 1)", "rgba(0, 0, 0, .6)");
		//this.chkPoints[i].drawValueText(context, 0, 0, "#000", "ritaglio");
	}

	// si hay mas de un checkpoint seleccionado...
	if (this.selectedChkPoints.length > 0)
	{
		var lastSelectedChkp = this.selectedChkPoints[this.selectedChkPoints.length - 1]

		// configuarar contexto
		context.strokeStyle = "rgba(181, 131, 55, 1)";
		context.lineWidth = 8;
        context.lineCap = 'round';

		// dibujar linea entre lastSelectedChkp y mouse
		context.beginPath();
		context.moveTo(lastSelectedChkp.x, lastSelectedChkp.y);
		context.lineTo(mouseX, mouseY);
		context.stroke();
		context.closePath();

		// dibujar circulo alrededor del mouse
		context.beginPath();
		context.arc(mouseX, mouseY, 20, 0, Math.PI*2, false);
		context.fill();
		context.closePath();
	}

	// si se han seleccionado dos checkpoints...
	if (this.linesHistorial.length == 2)
	{
		// configurar contexto
		this.backContext.strokeStyle = "#000";
		this.backContext.strokeStyle = "rgba(181, 131, 55, 1)";
		this.backContext.lineWidth = 5;
		
		// dibujar lineas entre checkpoints
		this.backContext.moveTo(this.selectedChkPoints[this.selectedChkPoints.length - 2].x, 
						this.selectedChkPoints[this.selectedChkPoints.length - 2].y);
		this.backContext.lineTo(this.selectedChkPoints[this.selectedChkPoints.length - 1].x, 
						this.selectedChkPoints[this.selectedChkPoints.length - 1].y);
		this.backContext.stroke();
	}
	
	this.drawHelpLines(context);
	context.globalAlpha = this.alphaSolution;
	context.drawImage(this.imageSolution, 0, 0);
	// restablece el valor a su estado anterior
	context.globalAlpha = 1;
	// dibujar imagen q mantiene el registro de las lineas seleccionadas
	context.drawImage(this.backCanvas, 0, 0);
}

MainScene.prototype.update = function() 
{
	if  (this. alphaSolution > 0)
		this.alphaSolution -= this.alphaDelta;
	else 
		this.alphaSolution = 0;                              

	for (var i = 0; i < this.chkPoints.length; i++)
	{
		this.chkPoints[i].update();
		if (this.chkPoints[i].isChecked && !this.chkPoints[i].isIterated) 
		{
			this.chkPoints[i].isIterated = true;
			this.selectedChkPoints.push(this.chkPoints[i]);
			
			// mantiene registro de los dos ultimos checkpoints seleccionados
			this.linesHistorial.push(this.chkPoints[i]);
		}
	}

	// cuando linesHistorial tiene mas de dos elementos...
	if (this.linesHistorial.length > 2)
	{
		// remover del array el primer chkpoint ingresado e ingresarle
		// el penultimo y el ultimo chkpoint seleccionado
		var lines_tmp = this.linesHistorial;
		this.linesHistorial = [];
		this.linesHistorial.push(lines_tmp[lines_tmp.length - 2]);
		this.linesHistorial.push(lines_tmp[lines_tmp.length - 1]);
	}

	if (!isMouseDown) 
	{
		if (this.selectedChkPoints.length != this.password.length && 
			this.selectedChkPoints.length > 0) 
		{

			currentScene = 3;
			return;
		}

		if (this.selectedChkPoints.length == this.password.length)
		{
			for (var i = 0; i < this.password.length; i++) 
			{
				this.isCorrectPassword.push((this.selectedChkPoints[i].key == this.password[i]) ? true : false);
				console.log(this.isCorrectPassword);
			}
		}

		// que hacer cuando se gana o se pierde
		for (var i = 0; i < this.isCorrectPassword.length; i++)
		{
			if (!this.isCorrectPassword[i]) 
			{
				// perdiste el juego
				currentScene = 3;
				this.selectedChkPoints = [];
				break;
	        } 

        	// ganaste el juego
        	currentScene = 2;
		}

		this.selectedChkPoints = [];
	}

}


MainScene.prototype.drawSolution = function(ctx) 
{
	// configuar contexto
	ctx.strokeStyle = "#fff";
	ctx.lineWidth = 5;
	drawLineBetween(this.chkPoints[1], this.chkPoints[6], ctx);
	drawLineBetween(this.chkPoints[6], this.chkPoints[11], ctx);
	drawLineBetween(this.chkPoints[11], this.chkPoints[15], ctx);
	drawLineBetween(this.chkPoints[15], this.chkPoints[20], ctx);
	drawLineBetween(this.chkPoints[20], this.chkPoints[26], ctx);
	drawLineBetween(this.chkPoints[26], this.chkPoints[27], ctx);
	drawLineBetween(this.chkPoints[27], this.chkPoints[21], ctx);
	drawLineBetween(this.chkPoints[21], this.chkPoints[16], ctx);
	drawLineBetween(this.chkPoints[16], this.chkPoints[12], ctx);
	drawLineBetween(this.chkPoints[12], this.chkPoints[17], ctx);
	drawLineBetween(this.chkPoints[17], this.chkPoints[22], ctx);
	drawLineBetween(this.chkPoints[22], this.chkPoints[18], ctx);
	drawLineBetween(this.chkPoints[18], this.chkPoints[14], ctx);
	drawLineBetween(this.chkPoints[14], this.chkPoints[9], ctx);
	drawLineBetween(this.chkPoints[9], this.chkPoints[8], ctx);
	drawLineBetween(this.chkPoints[8], this.chkPoints[13], ctx);
	ctx.stroke();
};

MainScene.prototype.drawHelpLines = function(ctx) 
{
	//23, 24, 25, 28, 29
	var chkp2 = this.chkPoints[2];
	var chkp7 = this.chkPoints[7];
	var chkp12 = this.chkPoints[12];
	var chkp22 = this.chkPoints[22];
	var chkp23 = this.chkPoints[23];
	var chkp24 = this.chkPoints[24];
	var chkp27 = this.chkPoints[27];
	var chkp28 = this.chkPoints[28];

	// configuar contexto
	ctx.strokeStyle = "rgba(186, 134, 56, 1)";
	ctx.lineWidth = 5;

	// dibuja linea entre chkp7 y chkp2
	drawLineBetween(chkp2, chkp7, ctx);

	// dibuja linea entre chkp7 y chkp12
	drawLineBetween(chkp7, chkp12, ctx);

	drawLineBetween(chkp22, chkp23, ctx);
	drawLineBetween(chkp23, chkp24, ctx);
	drawLineBetween(chkp22, chkp27, ctx);
	drawLineBetween(chkp23, chkp28, ctx);
	drawLineBetween(chkp24, chkp28, ctx);
	// stroke las lineas
	ctx.stroke();
};

function drawLineBetween(point1, point2, ctx)
{
	ctx.moveTo(point1.x, point1.y);
	ctx.lineTo(point1.x + (point2.x - point1.x),
				point1.y + (point2.y - point1.y));
}

function VictoryScene()
{}

VictoryScene.prototype.draw = function(context) 
{
context.fillStyle = "#2ecc71";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "40pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Ve por la puerta verde!!!", 210, 370);
};

VictoryScene.prototype.update = function()
{}

function DefeatScene()
{}

DefeatScene.prototype.draw = function(context) 
{
	context.fillStyle = "#e74c3c";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "40pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Ve por la puerta roja!!!", 210, 370);
};

DefeatScene.prototype.update = function()
{}


function InstructionsScene()
{}

InstructionsScene.prototype.draw = function(context) 
{
	context.fillStyle = "#e74c3c";
	context.fillRect(0, 0, canvas.width, canvas.height);

	context.font = "45pt ritaglio";
	context.fillStyle = "#000";
	context.fillText("Instrucciones", 230, 177);
	context.fillStyle = "#fff";
	context.fillText("Instrucciones", 230, 175);

	context.font = "18pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("Dejar presionado el mouse y seleccionar las personas", 245, 200);
	context.fillText("en el orden que memorizaste los circilos verdes", 245, 225);
	//las personas en el orden que memorizaste los circilos verdes

	context.font = "14pt ritaglio";
	context.fillStyle = "#fff";
	context.fillText("(Presiona space para empezar)", 245, 300);
};

InstructionsScene.prototype.update = function()
{}